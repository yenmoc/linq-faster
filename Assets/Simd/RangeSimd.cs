﻿using System.Numerics;
using UnityModule.Ex;

// ReSharper disable UnusedMember.Global
// ReSharper disable once CheckNamespace
namespace UnityModule.LinqFaster.Simd
{
    public static partial class LinqLightSimd
    {
        /// <summary>
        /// Generates a sequence of integral numbers within a specified range
        /// using SIMD
        /// </summary>
        /// <param name="start">The value of the first integer in the sequence.</param>
        /// <param name="len">The number of sequential integers to generate.</param>
        /// <returns>A sequence that contains a range of sequential integral numbers.</returns>
        public static int[] RangeSimd(int start, int len)
        {
            long max = ((long) start) + len - 1;
            if (len < 0 || max > int.MaxValue)
            {
                throw Error.ArgumentOutOfRange("len");
            }

            var count = Vector<int>.Count;
            int[] result = new int[len];
            if (len >= count)
            {
                //use result array for double duty to save memory

                for (int i = 0; i < count; i++)
                {
                    result[i] = i + start;
                }

                var v = new Vector<int>(result);
                var increment = new Vector<int>(count);
                v = v + increment;
                for (int i = count; i <= len - count; i += count)
                {
                    v.CopyTo(result, i);
                    v = v + increment;
                }
            }

            for (int i = len - len % count; i < len; i++)
            {
                result[i] = i + start;
            }

            return result;
        }
    }
}