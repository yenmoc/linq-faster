﻿using System.Numerics;
using UnityModule.Ex;
using static UnityModule.LinqFaster.Utils.GenericOperators;

// ReSharper disable UnusedMember.Global

// ReSharper disable once CheckNamespace
namespace UnityModule.LinqFaster.Simd
{
    public static partial class LinqLightSimd
    {
        /// <summary>
        /// Finds the minimum value in a sequence using SIMD.
        /// </summary>        
        /// <param name="source">A sequence of primitive values</param>
        /// <returns>The minimum value in the sequence</returns>
        public static T MinSimd<T>(this T[] source)
            where T : struct
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            T min = source[0];
            int count = Vector<T>.Count;
            if (count <= source.Length)
            {
                var vMin = new Vector<T>(source, 0);
                for (int i = count; i <= source.Length - count; i += count)
                {
                    var v = new Vector<T>(source, i);
                    vMin = Vector.Min(v, vMin);
                }

                min = vMin[0];
                for (int i = 1; i < count; i++)
                {
                    if (LessThan(vMin[i], min)) min = vMin[i];
                }
            }

            for (int i = source.Length - source.Length % count; i < source.Length; i++)
            {
                if (LessThan(source[i], min)) min = source[i];
            }

            return min;
        }
    }
}