﻿using System;
using System.Numerics;
using static UnityModule.LinqFaster.Utils.GenericOperators;

// ReSharper disable InconsistentNaming
// ReSharper disable UnusedMember.Global
// ReSharper disable CheckNamespace
namespace UnityModule.LinqFaster.Simd
{
    /// <summary>
    /// SIMD accelerated Linq-style extension methods for arrays.
    /// </summary>
    public static partial class LinqLightSimd
    {
        /// <summary>
        /// Calculates the average value of an array using SIMD.
        /// </summary>        
        /// <param name="source">The sequence of primitive values to average</param>
        /// <returns>The average of the sequence of values as a double</returns>
        public static double AverageSimd<T>(this T[] source) where T : struct
        {
            return Divide(SumSimd(source), source.Length);
        }

        /// <summary>
        /// Calculates the average value of an array via a selector using SIMD.
        /// </summary>        
        /// <param name="source">The sequence of primitive values to transform then average</param>
        /// <param name="selectorSimd">A transformation function to operate on vectors.</param>
        /// <param name="selector">A transformation function to operate on leftover elements.</param>
        /// <returns>The average of the transformed sequence of values as a double</returns>
        public static double AverageSimd<T, U>(this T[] source, Func<Vector<T>, Vector<U>> selectorSimd, Func<T, U> selector = null)
            where T : struct
            where U : struct
        {
            return Divide(SumSimd(source, selectorSimd, selector), source.Length);
        }

        /// <summary>
        /// Calculates the average value of an array using SIMD.
        /// </summary>        
        /// <param name="source">The sequence of primitive values to average</param>
        /// <returns>The average of the sequence of values as a float</returns>
        public static float AverageSimsFloat<T>(this T[] source) where T : struct
        {
            return DivideFloat(SumSimd(source), source.Length);
        }

        /// <summary>
        /// Calculates the average value of an array via a selector using SIMD.
        /// </summary>        
        /// <param name="source">The sequence of primitive values to transform then average</param>
        /// <param name="selectorSimd">A transformation function to operate on vectors.</param>
        /// <param name="selector">A transformation function to operate on leftover elements.</param>
        /// <returns>The average of the transformed sequence of values as a double</returns>
        public static float AverageSimdFloat<T, U>(this T[] source, Func<Vector<T>, Vector<U>> selectorSimd, Func<T, U> selector = null)
            where T : struct
            where U : struct
        {
            return DivideFloat(SumSimd(source, selectorSimd, selector), source.Length);
        }
    }
}