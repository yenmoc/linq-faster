﻿using System;
using System.Numerics;
using UnityModule.Ex;

// ReSharper disable InconsistentNaming
// ReSharper disable UnusedMember.Global
// ReSharper disable once CheckNamespace
namespace UnityModule.LinqFaster.Simd
{
    public static partial class LinqLightSimd
    {
        /// <summary>
        /// Projects each element of a sequence into a new form with SIMD.
        /// </summary>        
        /// <param name="source">The sequence of primitive values to transform.</param>
        /// <param name="slectorSimd">A transformation function that operates on Vectors.</param>
        /// <param name="selector">An optional selection function to transform the leftover values.</param>
        /// <returns>A sequence of transformed values.</returns>        
        public static U[] SelectSimd<T, U>(this T[] source, Func<Vector<T>, Vector<U>> slectorSimd, Func<T, U> selector = null)
            where T : struct
            where U : struct
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (slectorSimd == null)
            {
                throw Error.ArgumentNull("selectorSIMD");
            }

            var count = Vector<T>.Count;

            if (count != Vector<U>.Count)
            {
                throw Error.ArgumentOutOfRange("selectorSIMD");
            }

            var result = new U[source.Length];

            int i = 0;
            for (; i <= source.Length - count; i += count)
            {
                slectorSimd(new Vector<T>(source, i)).CopyTo(result, i);
            }

            if (selector != null)
            {
                i = source.Length - source.Length % count;
                for (; i < result.Length; i++)
                {
                    result[i] = selector(source[i]);
                }
            }

            return result;
        }

        /// <summary>
        /// Selects/Maps in place each element of a sequence into a new form with SIMD.
        /// </summary>        
        /// <param name="source">The sequence of primitive values to transform.</param>
        /// <param name="slectorSimd">A transformation function that operates on Vectors.</param>
        /// <param name="selector">An optional selection function to transform the leftover values.</param>        
        public static void SelectInPlaceSimd<T>(this T[] source, Func<Vector<T>, Vector<T>> slectorSimd, Func<T, T> selector = null)
            where T : struct
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (slectorSimd == null)
            {
                throw Error.ArgumentNull("selectorSIMD");
            }

            var count = Vector<T>.Count;


            int i = 0;
            for (; i <= source.Length - count; i += count)
            {
                slectorSimd(new Vector<T>(source, i)).CopyTo(source, i);
            }

            if (selector != null)
            {
                i = source.Length - source.Length % count;
                for (; i < source.Length; i++)
                {
                    source[i] = selector(source[i]);
                }
            }
        }
    }
}