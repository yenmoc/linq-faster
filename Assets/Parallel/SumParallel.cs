﻿using System;
using System.Collections.Generic;
using System.Threading;
using UnityModule.Ex;
using static UnityModule.LinqFaster.Utils.CustomPartition;

// ReSharper disable UnusedMember.Global
// ReSharper disable once CheckNamespace
namespace UnityModule.LinqFaster.Parallel
{
    public static partial class LinqLightParallel
    {
        #region ------------------------------ Arrays ------------------------------

        /// <summary>
        ///  Adds a sequence of values using multiple Tasks / Threads.
        /// </summary>
        /// <param name="source">The sequence to add.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The sum of the sequence.</returns>
        public static int SumParallel(this int[] source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            int sum = 0;
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0,
                (range, s, acc) =>
                {
                    checked
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            acc += source[i];
                        }
                    }

                    return acc;
                },
                acc => Interlocked.Add(ref sum, acc));

            return sum;
        }

        /// <summary>
        /// Adds the transformed sequence of elements using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">The sequence of values to transform then sum.</param>
        /// <param name="selector">A transformation function.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The sum of the transformed elements.</returns>
        public static int SumParallel<T>(this T[] source, Func<T, int> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (selector == null)
            {
                throw Error.ArgumentNull("selector");
            }

            int sum = 0;
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0,
                (range, s, acc) =>
                {
                    checked
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            acc += selector(source[i]);
                        }
                    }

                    return acc;
                },
                acc => Interlocked.Add(ref sum, acc));

            return sum;
        }

        /// <summary>
        ///  Adds a sequence of values using multiple Tasks / Threads.
        /// </summary>
        /// <param name="source">The sequence to add.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The sum of the sequence.</returns>
        public static long SumParallel(this long[] source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            long sum = 0;
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0L,
                (range, s, acc) =>
                {
                    checked
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            acc += source[i];
                        }
                    }

                    return acc;
                },
                acc => Interlocked.Add(ref sum, acc));

            return sum;
        }

        /// <summary>
        /// Adds the transformed sequence of elements using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">The sequence of values to transform then sum.</param>
        /// <param name="selector">A transformation function.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The sum of the transformed elements.</returns>
        public static long SumParallel<T>(this T[] source, Func<T, long> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (selector == null)
            {
                throw Error.ArgumentNull("selector");
            }

            long sum = 0;
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0L,
                (range, s, acc) =>
                {
                    checked
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            acc += selector(source[i]);
                        }
                    }

                    return acc;
                },
                acc => Interlocked.Add(ref sum, acc));

            return sum;
        }

        /// <summary>
        ///  Adds a sequence of values using multiple Tasks / Threads.
        /// </summary>
        /// <param name="source">The sequence to add.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The sum of the sequence.</returns>
        public static float SumParallel(this float[] source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            double sum = 0;
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += source[i];
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });

            return (float) sum;
        }

        /// <summary>
        /// Adds the transformed sequence of elements using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">The sequence of values to transform then sum.</param>
        /// <param name="selector">A transformation function.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The sum of the transformed elements.</returns>
        public static float SumParallel<T>(this T[] source, Func<T, float> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (selector == null)
            {
                throw Error.ArgumentNull("selector");
            }

            double sum = 0;
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += selector(source[i]);
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });


            return (float) sum;
        }

        /// <summary>
        ///  Adds a sequence of values using multiple Tasks / Threads.
        /// </summary>
        /// <param name="source">The sequence to add.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The sum of the sequence.</returns>
        public static double SumParallel(this double[] source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            double sum = 0;
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += source[i];
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });


            return sum;
        }

        /// <summary>
        /// Adds the transformed sequence of elements using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">The sequence of values to transform then sum.</param>
        /// <param name="selector">A transformation function.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The sum of the transformed elements.</returns>
        public static double SumParallel<T>(this T[] source, Func<T, double> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (selector == null)
            {
                throw Error.ArgumentNull("selector");
            }

            double sum = 0;
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += selector(source[i]);
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });


            return sum;
        }

        /// <summary>
        ///  Adds a sequence of values using multiple Tasks / Threads.
        /// </summary>
        /// <param name="source">The sequence to add.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The sum of the sequence.</returns>
        public static decimal SumParallel(this decimal[] source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            decimal sum = 0;
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => (decimal) 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += source[i];
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });


            return sum;
        }

        /// <summary>
        /// Adds the transformed sequence of elements using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">The sequence of values to transform then sum.</param>
        /// <param name="selector">A transformation function.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The sum of the transformed elements.</returns>
        public static decimal SumParallel<T>(this T[] source, Func<T, decimal> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (selector == null)
            {
                throw Error.ArgumentNull("selector");
            }

            decimal sum = 0;
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => (decimal) 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += selector(source[i]);
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });

            return sum;
        }

        #endregion

        #region ------------------------------ Lists ------------------------------

        /// <summary>
        ///  Adds a sequence of values using multiple Tasks / Threads.
        /// </summary>
        /// <param name="source">The sequence to add.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The sum of the sequence.</returns>
        public static int SumParallel(this List<int> source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            int sum = 0;
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0,
                (range, s, acc) =>
                {
                    checked
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            acc += source[i];
                        }
                    }

                    return acc;
                },
                acc => Interlocked.Add(ref sum, acc));

            return sum;
        }

        /// <summary>
        /// Adds the transformed sequence of elements using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">The sequence of values to transform then sum.</param>
        /// <param name="selector">A transformation function.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The sum of the transformed elements.</returns>
        public static int SumParallel<T>(this List<T> source, Func<T, int> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (selector == null)
            {
                throw Error.ArgumentNull("selector");
            }

            int sum = 0;
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0,
                (range, s, acc) =>
                {
                    checked
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            acc += selector(source[i]);
                        }
                    }

                    return acc;
                },
                acc => Interlocked.Add(ref sum, acc));

            return sum;
        }

        /// <summary>
        ///  Adds a sequence of values using multiple Tasks / Threads.
        /// </summary>
        /// <param name="source">The sequence to add.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The sum of the sequence.</returns>
        public static long SumParallel(this List<long> source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            long sum = 0;
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0L,
                (range, s, acc) =>
                {
                    checked
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            acc += source[i];
                        }
                    }

                    return acc;
                },
                acc => Interlocked.Add(ref sum, acc));

            return sum;
        }

        /// <summary>
        /// Adds the transformed sequence of elements using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">The sequence of values to transform then sum.</param>
        /// <param name="selector">A transformation function.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The sum of the transformed elements.</returns>
        public static long SumParallel<T>(this List<T> source, Func<T, long> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (selector == null)
            {
                throw Error.ArgumentNull("selector");
            }

            long sum = 0;
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0L,
                (range, s, acc) =>
                {
                    checked
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            acc += selector(source[i]);
                        }
                    }

                    return acc;
                },
                acc => Interlocked.Add(ref sum, acc));

            return sum;
        }

        /// <summary>
        ///  Adds a sequence of values using multiple Tasks / Threads.
        /// </summary>
        /// <param name="source">The sequence to add.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The sum of the sequence.</returns>
        public static float SumParallel(this List<float> source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            double sum = 0;
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += source[i];
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });

            return (float) sum;
        }

        /// <summary>
        /// Adds the transformed sequence of elements using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">The sequence of values to transform then sum.</param>
        /// <param name="selector">A transformation function.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The sum of the transformed elements.</returns>
        public static float SumParallel<T>(this List<T> source, Func<T, float> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (selector == null)
            {
                throw Error.ArgumentNull("selector");
            }

            double sum = 0;
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += selector(source[i]);
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });

            return (float) sum;
        }

        /// <summary>
        ///  Adds a sequence of values using multiple Tasks / Threads.
        /// </summary>
        /// <param name="source">The sequence to add.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The sum of the sequence.</returns>
        public static double SumParallel(this List<double> source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            double sum = 0;
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += source[i];
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });

            return sum;
        }

        /// <summary>
        /// Adds the transformed sequence of elements using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">The sequence of values to transform then sum.</param>
        /// <param name="selector">A transformation function.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The sum of the transformed elements.</returns>
        public static double SumParallel<T>(this List<T> source, Func<T, double> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (selector == null)
            {
                throw Error.ArgumentNull("selector");
            }

            double sum = 0;
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0.0,
                (range, s, acc) =>
                {
                    checked
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            acc += selector(source[i]);
                        }
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });

            return sum;
        }

        /// <summary>
        ///  Adds a sequence of values using multiple Tasks / Threads.
        /// </summary>
        /// <param name="source">The sequence to add.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The sum of the sequence.</returns>
        public static decimal SumParallel(this List<decimal> source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            decimal sum = 0;

            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => (decimal) 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += source[i];
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });


            return sum;
        }

        /// <summary>
        /// Adds the transformed sequence of elements using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">The sequence of values to transform then sum.</param>
        /// <param name="selector">A transformation function.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The sum of the transformed elements.</returns>
        public static decimal SumParallel<T>(this List<T> source, Func<T, decimal> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (selector == null)
            {
                throw Error.ArgumentNull("selector");
            }

            decimal sum = 0;
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => (decimal) 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += selector(source[i]);
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });

            return sum;
        }

        #endregion
    }
}