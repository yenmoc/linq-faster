﻿using System;
using System.Collections.Generic;
using UnityModule.Ex;
using static UnityModule.LinqFaster.Utils.CustomPartition;

// ReSharper disable UnusedMember.Global
// ReSharper disable once CheckNamespace
namespace UnityModule.LinqFaster.Parallel
{
    public static partial class LinqLightParallel
    {
        #region ------------------------------ Arrays ------------------------------

        /// <summary>
        /// Returns the Maximum value in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static T MaxParallel<T>(this T[] source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            Comparer<T> comparer = Comparer<T>.Default;
            T max = default(T);
            if (max == null)
            {
                max = source[0];
                System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                    () => max,
                    (range, state, threadMax) =>
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            if (source[i] != null && comparer.Compare(source[i], threadMax) > 0) threadMax = source[i];
                        }

                        return threadMax;
                    },
                    threadMax =>
                    {
                        lock (LOCK)
                        {
                            if (threadMax != null && comparer.Compare(threadMax, max) > 0)
                            {
                                max = threadMax;
                            }
                        }
                    });
            }
            else
            {
                max = source[0];
                System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                    () => max,
                    (range, state, threadMax) =>
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            if (comparer.Compare(source[i], threadMax) > 0) threadMax = source[i];
                        }

                        return threadMax;
                    },
                    threadMax =>
                    {
                        lock (LOCK)
                        {
                            if (comparer.Compare(threadMax, max) > 0)
                            {
                                max = threadMax;
                            }
                        }
                    });
            }

            return max;
        }

        /// <summary>
        /// Returns the Maximum value via the selector in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="selector">A function to transform elements into a value that will be compared.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static TResult MaxParallel<T, TResult>(this T[] source, Func<T, TResult> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            Comparer<TResult> comparer = Comparer<TResult>.Default;
            var max = default(TResult);
            if (max == null)
            {
                max = selector(source[0]);
                System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                    () => max,
                    (range, state, threadMax) =>
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            var s = selector(source[i]);
                            if (s != null && comparer.Compare(s, threadMax) > 0) threadMax = s;
                        }

                        return threadMax;
                    },
                    threadMax =>
                    {
                        lock (LOCK)
                        {
                            if (threadMax != null && comparer.Compare(threadMax, max) > 0)
                            {
                                max = threadMax;
                            }
                        }
                    });
            }
            else
            {
                max = selector(source[0]);
                System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                    () => max,
                    (range, state, threadMax) =>
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            var s = selector(source[i]);
                            if (comparer.Compare(s, threadMax) > 0) threadMax = s;
                        }

                        return threadMax;
                    },
                    threadMax =>
                    {
                        lock (LOCK)
                        {
                            if (comparer.Compare(threadMax, max) > 0)
                            {
                                max = threadMax;
                            }
                        }
                    });
            }

            return max;
        }

        /// <summary>
        /// Returns the Maximum value in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static int MaxParallel(this int[] source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            int max = source[0];
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => max,
                (range, state, threadMax) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        if (source[i] > threadMax)
                        {
                            threadMax = source[i];
                        }
                    }

                    return threadMax;
                },
                threadMax =>
                {
                    lock (LOCK)
                    {
                        if (threadMax > max)
                        {
                            max = threadMax;
                        }
                    }
                });

            return max;
        }

        /// <summary>
        /// Returns the Maximum value via the selector in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="selector">A function to transform elements into a value that will be compared.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static int MaxParallel<T>(this T[] source, Func<T, int> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            int max = selector(source[0]);
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => max,
                (range, state, threadMax) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        var s = selector(source[i]);
                        if (s > threadMax)
                        {
                            threadMax = s;
                        }
                    }

                    return threadMax;
                },
                threadMax =>
                {
                    lock (LOCK)
                    {
                        if (threadMax > max)
                        {
                            max = threadMax;
                        }
                    }
                });

            return max;
        }

        /// <summary>
        /// Returns the Maximum value in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static long MaxParallel(this long[] source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            long max = source[0];
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => max,
                (range, state, threadMax) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        if (source[i] > threadMax)
                        {
                            threadMax = source[i];
                        }
                    }

                    return threadMax;
                },
                threadMax =>
                {
                    lock (LOCK)
                    {
                        if (threadMax > max)
                        {
                            max = threadMax;
                        }
                    }
                });

            return max;
        }

        /// <summary>
        /// Returns the Maximum value via the selector in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="selector">A function to transform elements into a value that will be compared.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static long MaxParallel<T>(this T[] source, Func<T, long> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            long max = selector(source[0]);
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => max,
                (range, state, threadMax) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        var s = selector(source[i]);
                        if (s > threadMax)
                        {
                            threadMax = s;
                        }
                    }

                    return threadMax;
                },
                threadMax =>
                {
                    lock (LOCK)
                    {
                        if (threadMax > max)
                        {
                            max = threadMax;
                        }
                    }
                });

            return max;
        }

        /// <summary>
        /// Returns the Maximum value in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static float MaxParallel(this float[] source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            float max = source[0];
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => max,
                (range, state, threadMax) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        if (source[i] > threadMax)
                        {
                            threadMax = source[i];
                        }
                    }

                    return threadMax;
                },
                threadMax =>
                {
                    lock (LOCK)
                    {
                        if (threadMax > max)
                        {
                            max = threadMax;
                        }
                    }
                });

            return max;
        }

        /// <summary>
        /// Returns the Maximum value via the selector in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="selector">A function to transform elements into a value that will be compared.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static float MaxParallel<T>(this T[] source, Func<T, float> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            float max = selector(source[0]);
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => max,
                (range, state, threadMax) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        var s = selector(source[i]);
                        if (s > threadMax)
                        {
                            threadMax = s;
                        }
                    }

                    return threadMax;
                },
                threadMax =>
                {
                    lock (LOCK)
                    {
                        if (threadMax > max)
                        {
                            max = threadMax;
                        }
                    }
                });

            return max;
        }

        /// <summary>
        /// Returns the Maximum value in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static double MaxParallel(this double[] source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            double max = source[0];
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => max,
                (range, state, threadMax) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        if (source[i] > threadMax)
                        {
                            threadMax = source[i];
                        }
                    }

                    return threadMax;
                },
                threadMax =>
                {
                    lock (LOCK)
                    {
                        if (threadMax > max)
                        {
                            max = threadMax;
                        }
                    }
                });

            return max;
        }

        /// <summary>
        /// Returns the Maximum value via the selector in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="selector">A function to transform elements into a value that will be compared.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static double MaxParallel<T>(this T[] source, Func<T, double> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            double max = selector(source[0]);
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => max,
                (range, state, threadMax) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        var s = selector(source[i]);
                        if (s > threadMax)
                        {
                            threadMax = s;
                        }
                    }

                    return threadMax;
                },
                threadMax =>
                {
                    lock (LOCK)
                    {
                        if (threadMax > max)
                        {
                            max = threadMax;
                        }
                    }
                });

            return max;
        }

        /// <summary>
        /// Returns the Maximum value in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static decimal MaxParallel(this decimal[] source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            decimal max = source[0];
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => max,
                (range, state, threadMax) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        if (source[i] > threadMax)
                        {
                            threadMax = source[i];
                        }
                    }

                    return threadMax;
                },
                threadMax =>
                {
                    lock (LOCK)
                    {
                        if (threadMax > max)
                        {
                            max = threadMax;
                        }
                    }
                });

            return max;
        }

        /// <summary>
        /// Returns the Maximum value via the selector in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="selector">A function to transform elements into a value that will be compared.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static decimal MaxParallel<T>(this T[] source, Func<T, decimal> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            decimal max = selector(source[0]);
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => max,
                (range, state, threadMax) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        var s = selector(source[i]);
                        if (s > threadMax)
                        {
                            threadMax = s;
                        }
                    }

                    return threadMax;
                },
                threadMax =>
                {
                    lock (LOCK)
                    {
                        if (threadMax > max)
                        {
                            max = threadMax;
                        }
                    }
                });

            return max;
        }

        #endregion

        #region ------------------------------ Lists ------------------------------

        /// <summary>
        /// Returns the Maximum value in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static T MaxParallel<T>(this List<T> source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Count == 0)
            {
                throw Error.NoElements();
            }

            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            Comparer<T> comparer = Comparer<T>.Default;
            T max = default(T);
            if (max == null)
            {
                max = source[0];
                System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                    () => max,
                    (range, state, threadMax) =>
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            if (source[i] != null && comparer.Compare(source[i], threadMax) > 0) threadMax = source[i];
                        }

                        return threadMax;
                    },
                    threadMax =>
                    {
                        lock (LOCK)
                        {
                            if (threadMax != null && comparer.Compare(threadMax, max) > 0)
                            {
                                max = threadMax;
                            }
                        }
                    });
            }
            else
            {
                max = source[0];
                System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                    () => max,
                    (range, state, threadMax) =>
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            if (comparer.Compare(source[i], threadMax) > 0) threadMax = source[i];
                        }

                        return threadMax;
                    },
                    threadMax =>
                    {
                        lock (LOCK)
                        {
                            if (comparer.Compare(threadMax, max) > 0)
                            {
                                max = threadMax;
                            }
                        }
                    });
            }

            return max;
        }

        /// <summary>
        /// Returns the Maximum value via the selector in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="selector">A function to transform elements into a value that will be compared.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static TResult MaxParallel<T, TResult>(this List<T> source, Func<T, TResult> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Count == 0)
            {
                throw Error.NoElements();
            }

            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            Comparer<TResult> comparer = Comparer<TResult>.Default;
            var max = default(TResult);
            if (max == null)
            {
                max = selector(source[0]);
                System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                    () => max,
                    (range, state, threadMax) =>
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            var s = selector(source[i]);
                            if (s != null && comparer.Compare(s, threadMax) > 0) threadMax = s;
                        }

                        return threadMax;
                    },
                    threadMax =>
                    {
                        lock (LOCK)
                        {
                            if (threadMax != null && comparer.Compare(threadMax, max) > 0)
                            {
                                max = threadMax;
                            }
                        }
                    });
            }
            else
            {
                max = selector(source[0]);
                System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                    () => max,
                    (range, state, threadMax) =>
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            var s = selector(source[i]);
                            if (comparer.Compare(s, threadMax) > 0) threadMax = s;
                        }

                        return threadMax;
                    },
                    threadMax =>
                    {
                        lock (LOCK)
                        {
                            if (comparer.Compare(threadMax, max) > 0)
                            {
                                max = threadMax;
                            }
                        }
                    });
            }

            return max;
        }

        /// <summary>
        /// Returns the Maximum value in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static int MaxParallel(this List<int> source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Count == 0)
            {
                throw Error.NoElements();
            }

            int max = source[0];
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => max,
                (range, state, threadMax) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        if (source[i] > threadMax)
                        {
                            threadMax = source[i];
                        }
                    }

                    return threadMax;
                },
                threadMax =>
                {
                    lock (LOCK)
                    {
                        if (threadMax > max)
                        {
                            max = threadMax;
                        }
                    }
                });

            return max;
        }

        /// <summary>
        /// Returns the Maximum value via the selector in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="selector">A function to transform elements into a value that will be compared.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static int MaxParallel<T>(this List<T> source, Func<T, int> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Count == 0)
            {
                throw Error.NoElements();
            }

            int max = selector(source[0]);
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => max,
                (range, state, threadMax) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        var s = selector(source[i]);
                        if (s > threadMax)
                        {
                            threadMax = s;
                        }
                    }

                    return threadMax;
                },
                threadMax =>
                {
                    lock (LOCK)
                    {
                        if (threadMax > max)
                        {
                            max = threadMax;
                        }
                    }
                });

            return max;
        }

        /// <summary>
        /// Returns the Maximum value in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static long MaxParallel(this List<long> source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Count == 0)
            {
                throw Error.NoElements();
            }

            long max = source[0];
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => max,
                (range, state, threadMax) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        if (source[i] > threadMax)
                        {
                            threadMax = source[i];
                        }
                    }

                    return threadMax;
                },
                threadMax =>
                {
                    lock (LOCK)
                    {
                        if (threadMax > max)
                        {
                            max = threadMax;
                        }
                    }
                });

            return max;
        }

        /// <summary>
        /// Returns the Maximum value via the selector in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="selector">A function to transform elements into a value that will be compared.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static long MaxParallel<T>(this List<T> source, Func<T, long> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Count == 0)
            {
                throw Error.NoElements();
            }

            long max = selector(source[0]);
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => max,
                (range, state, threadMax) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        var s = selector(source[i]);
                        if (s > threadMax)
                        {
                            threadMax = s;
                        }
                    }

                    return threadMax;
                },
                threadMax =>
                {
                    lock (LOCK)
                    {
                        if (threadMax > max)
                        {
                            max = threadMax;
                        }
                    }
                });

            return max;
        }

        /// <summary>
        /// Returns the Maximum value in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static float MaxParallel(this List<float> source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Count == 0)
            {
                throw Error.NoElements();
            }

            float max = source[0];
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => max,
                (range, state, threadMax) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        if (source[i] > threadMax)
                        {
                            threadMax = source[i];
                        }
                    }

                    return threadMax;
                },
                threadMax =>
                {
                    lock (LOCK)
                    {
                        if (threadMax > max)
                        {
                            max = threadMax;
                        }
                    }
                });

            return max;
        }

        /// <summary>
        /// Returns the Maximum value via the selector in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="selector">A function to transform elements into a value that will be compared.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static float MaxParallel<T>(this List<T> source, Func<T, float> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Count == 0)
            {
                throw Error.NoElements();
            }

            float max = selector(source[0]);
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => max,
                (range, state, threadMax) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        var s = selector(source[i]);
                        if (s > threadMax)
                        {
                            threadMax = s;
                        }
                    }

                    return threadMax;
                },
                threadMax =>
                {
                    lock (LOCK)
                    {
                        if (threadMax > max)
                        {
                            max = threadMax;
                        }
                    }
                });

            return max;
        }

        /// <summary>
        /// Returns the Maximum value in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static double MaxParallel(this List<double> source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Count == 0)
            {
                throw Error.NoElements();
            }

            double max = source[0];
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => max,
                (range, state, threadMax) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        if (source[i] > threadMax)
                        {
                            threadMax = source[i];
                        }
                    }

                    return threadMax;
                },
                threadMax =>
                {
                    lock (LOCK)
                    {
                        if (threadMax > max)
                        {
                            max = threadMax;
                        }
                    }
                });

            return max;
        }

        /// <summary>
        /// Returns the Maximum value via the selector in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="selector">A function to transform elements into a value that will be compared.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static double MaxParallel<T>(this List<T> source, Func<T, double> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Count == 0)
            {
                throw Error.NoElements();
            }

            double max = selector(source[0]);
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => max,
                (range, state, threadMax) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        var s = selector(source[i]);
                        if (s > threadMax)
                        {
                            threadMax = s;
                        }
                    }

                    return threadMax;
                },
                threadMax =>
                {
                    lock (LOCK)
                    {
                        if (threadMax > max)
                        {
                            max = threadMax;
                        }
                    }
                });

            return max;
        }

        /// <summary>
        /// Returns the Maximum value in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static decimal MaxParallel(this List<decimal> source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Count == 0)
            {
                throw Error.NoElements();
            }

            decimal max = source[0];
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => max,
                (range, state, threadMax) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        if (source[i] > threadMax)
                        {
                            threadMax = source[i];
                        }
                    }

                    return threadMax;
                },
                threadMax =>
                {
                    lock (LOCK)
                    {
                        if (threadMax > max)
                        {
                            max = threadMax;
                        }
                    }
                });

            return max;
        }

        /// <summary>
        /// Returns the Maximum value via the selector in a sequence of values using multiple Tasks / Threads.
        /// </summary>        
        /// <param name="source">A sequence of values to determine the Maximum of.</param>
        /// <param name="selector">A function to transform elements into a value that will be compared.</param>
        /// <param name="batchSize">Optional. Specify a batch size for Tasks to operate over. </param>
        /// <returns>The Maximum value in the sequence</returns>
        public static decimal MaxParallel<T>(this List<T> source, Func<T, decimal> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Count == 0)
            {
                throw Error.NoElements();
            }

            decimal max = selector(source[0]);
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => max,
                (range, state, threadMax) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        var s = selector(source[i]);
                        if (s > threadMax)
                        {
                            threadMax = s;
                        }
                    }

                    return threadMax;
                },
                threadMax =>
                {
                    lock (LOCK)
                    {
                        if (threadMax > max)
                        {
                            max = threadMax;
                        }
                    }
                });

            return max;
        }

        #endregion
    }
}