﻿using System;
using System.Collections.Generic;
using System.Threading;
using UnityModule.Ex;
using static UnityModule.LinqFaster.Utils.CustomPartition;

// ReSharper disable UnusedMember.Global
// ReSharper disable once CheckNamespace
namespace UnityModule.LinqFaster.Parallel
{
    public static partial class LinqLightParallel
    {
        #region ------------------------------ Arrays ------------------------------

        /// <summary>
        /// Computes the average of a sequence in parallel.
        /// </summary>
        /// <param name="source">The array to calculate the average of.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The average of the array.</returns>
        public static double AverageParallel(this int[] source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            long sum = 0;
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0L,
                (range, s, acc) =>
                {
                    checked
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            acc += source[i];
                        }
                    }

                    return acc;
                },
                acc => Interlocked.Add(ref sum, acc));

            return (double) sum / source.Length;
        }

        /// <summary>
        /// Computes the average of values obtained by invoking a transform function on
        /// each element of the input array in parallel.
        /// </summary>
        /// <param name="source">The array to calculate the transformed average of.</param>
        /// <param name="selector">A transform function to apply to each element.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The average of the array.</returns>
        public static double AverageParallel<T>(this T[] source, Func<T, int> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            if (selector == null)
            {
                throw Error.ArgumentNull("selector");
            }

            long sum = 0;
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0L,
                (range, s, acc) =>
                {
                    checked
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            acc += selector(source[i]);
                        }
                    }

                    return acc;
                },
                acc => Interlocked.Add(ref sum, acc));

            return (double) sum / source.Length;
        }

        /// <summary>
        /// Computes the average of a sequence in parallel.
        /// </summary>
        /// <param name="source">The array to calculate the average of.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The average of the array.</returns>
        public static double AverageParallel(this long[] source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            long sum = 0;
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0L,
                (range, s, acc) =>
                {
                    checked
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            acc += source[i];
                        }
                    }

                    return acc;
                },
                acc => Interlocked.Add(ref sum, acc));

            return (double) sum / source.Length;
        }

        /// <summary>
        /// Computes the average of values obtained by invoking a transform function on
        /// each element of the input array in parallel.
        /// </summary>
        /// <param name="source">The array to calculate the transformed average of.</param>
        /// <param name="selector">A transform function to apply to each element.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The average of the array.</returns>
        public static double AverageParallel<T>(this T[] source, Func<T, long> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            if (selector == null)
            {
                throw Error.ArgumentNull("selector");
            }

            long sum = 0;
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0L,
                (range, s, acc) =>
                {
                    checked
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            acc += selector(source[i]);
                        }
                    }

                    return acc;
                },
                acc => Interlocked.Add(ref sum, acc));

            return (double) sum / source.Length;
        }

        /// <summary>
        /// Computes the average of a sequence in parallel.
        /// </summary>
        /// <param name="source">The array to calculate the average of.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The average of the array.</returns>
        public static float AverageParallel(this float[] source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            double sum = 0;
            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += source[i];
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });

            return (float) (sum / source.Length);
        }

        /// <summary>
        /// Computes the average of values obtained by invoking a transform function on
        /// each element of the input array in parallel.
        /// </summary>
        /// <param name="source">The array to calculate the transformed average of.</param>
        /// <param name="selector">A transform function to apply to each element.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The average of the array.</returns>
        public static float AverageParallel<T>(this T[] source, Func<T, float> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            if (selector == null)
            {
                throw Error.ArgumentNull("selector");
            }

            double sum = 0;

            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += selector(source[i]);
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });

            return (float) (sum / source.Length);
        }

        /// <summary>
        /// Computes the average of a sequence in parallel.
        /// </summary>
        /// <param name="source">The array to calculate the average of.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The average of the array.</returns>
        public static double AverageParallel(this double[] source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            double sum = 0;

            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += source[i];
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });

            return sum / source.Length;
        }

        /// <summary>
        /// Computes the average of values obtained by invoking a transform function on
        /// each element of the input array in parallel.
        /// </summary>
        /// <param name="source">The array to calculate the transformed average of.</param>
        /// <param name="selector">A transform function to apply to each element.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The average of the array.</returns>
        public static double AverageParallel<T>(this T[] source, Func<T, double> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            if (selector == null)
            {
                throw Error.ArgumentNull("selector");
            }

            double sum = 0;

            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += selector(source[i]);
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });

            return sum / source.Length;
        }

        /// <summary>
        /// Computes the average of a sequence in parallel.
        /// </summary>
        /// <param name="source">The array to calculate the average of.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The average of the array.</returns>
        public static decimal AverageParallel(this decimal[] source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            decimal sum = 0;

            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => (decimal) 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += source[i];
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });


            return sum / source.Length;
        }

        /// <summary>
        /// Computes the average of values obtained by invoking a transform function on
        /// each element of the input array in parallel.
        /// </summary>
        /// <param name="source">The array to calculate the transformed average of.</param>
        /// <param name="selector">A transform function to apply to each element.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The average of the array.</returns>
        public static decimal AverageParallel<T>(this T[] source, Func<T, decimal> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Length == 0)
            {
                throw Error.NoElements();
            }

            if (selector == null)
            {
                throw Error.ArgumentNull("selector");
            }


            decimal sum = 0;

            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Length, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => (decimal) 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += selector(source[i]);
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });


            return sum / source.Length;
        }

        #endregion

        #region ------------------------------ Lists ------------------------------

        /// <summary>
        /// Computes the average of a sequence in parallel.
        /// </summary>
        /// <param name="source">The array to calculate the average of.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The average of the array.</returns>
        public static double AverageParallel(this List<int> source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Count == 0)
            {
                throw Error.NoElements();
            }

            long sum = 0;
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0L,
                (range, s, acc) =>
                {
                    checked
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            acc += source[i];
                        }
                    }

                    return acc;
                },
                acc => Interlocked.Add(ref sum, acc));

            return (double) sum / source.Count;
        }

        /// <summary>
        /// Computes the average of values obtained by invoking a transform function on
        /// each element of the input array in parallel.
        /// </summary>
        /// <param name="source">The array to calculate the transformed average of.</param>
        /// <param name="selector">A transform function to apply to each element.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>        
        /// <returns>The average of the array.</returns>
        public static double AverageParallel<T>(this List<T> source, Func<T, int> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Count == 0)
            {
                throw Error.NoElements();
            }

            if (selector == null)
            {
                throw Error.ArgumentNull("selector");
            }

            long sum = 0;
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0L,
                (range, s, acc) =>
                {
                    checked
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            acc += selector(source[i]);
                        }
                    }

                    return acc;
                },
                acc => Interlocked.Add(ref sum, acc));
            return (double) sum / source.Count;
        }

        /// <summary>
        /// Computes the average of a sequence in parallel.
        /// </summary>
        /// <param name="source">The array to calculate the average of.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The average of the array.</returns>
        public static double AverageParallel(this List<long> source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Count == 0)
            {
                throw Error.NoElements();
            }

            long sum = 0;
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0L,
                (range, s, acc) =>
                {
                    checked
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            acc += source[i];
                        }
                    }

                    return acc;
                },
                acc => Interlocked.Add(ref sum, acc));

            return (double) sum / source.Count;
        }

        /// <summary>
        /// Computes the average of values obtained by invoking a transform function on
        /// each element of the input array in parallel.
        /// </summary>
        /// <param name="source">The array to calculate the transformed average of.</param>
        /// <param name="selector">A transform function to apply to each element.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The average of the array.</returns>
        public static double AverageParallel<T>(this List<T> source, Func<T, long> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Count == 0)
            {
                throw Error.NoElements();
            }

            if (selector == null)
            {
                throw Error.ArgumentNull("selector");
            }

            long sum = 0;
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0L,
                (range, s, acc) =>
                {
                    checked
                    {
                        for (int i = range.Item1; i < range.Item2; i++)
                        {
                            acc += selector(source[i]);
                        }
                    }

                    return acc;
                },
                acc => Interlocked.Add(ref sum, acc));
            return (double) sum / source.Count;
        }

        /// <summary>
        /// Computes the average of a sequence in parallel.
        /// </summary>
        /// <param name="source">The array to calculate the average of.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The average of the array.</returns>
        public static double AverageParallel(this List<float> source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Count == 0)
            {
                throw Error.NoElements();
            }

            double sum = 0;

            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += source[i];
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });
            return sum / source.Count;
        }

        /// <summary>
        /// Computes the average of values obtained by invoking a transform function on
        /// each element of the input array in parallel.
        /// </summary>
        /// <param name="source">The array to calculate the transformed average of.</param>
        /// <param name="selector">A transform function to apply to each element.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The average of the array.</returns>
        public static double AverageParallel<T>(this List<T> source, Func<T, float> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Count == 0)
            {
                throw Error.NoElements();
            }

            if (selector == null)
            {
                throw Error.ArgumentNull("selector");
            }

            double sum = 0;

            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += selector(source[i]);
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });

            return sum / source.Count;
        }

        /// <summary>
        /// Computes the average of a sequence in parallel.
        /// </summary>
        /// <param name="source">The array to calculate the average of.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The average of the array.</returns>
        public static double AverageParallel(this List<double> source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Count == 0)
            {
                throw Error.NoElements();
            }

            double sum = 0;

            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += source[i];
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });

            return sum / source.Count;
        }

        /// <summary>
        /// Computes the average of values obtained by invoking a transform function on
        /// each element of the input array in parallel.
        /// </summary>
        /// <param name="source">The array to calculate the transformed average of.</param>
        /// <param name="selector">A transform function to apply to each element.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The average of the array.</returns>
        public static double AverageParallel<T>(this List<T> source, Func<T, double> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Count == 0)
            {
                throw Error.NoElements();
            }

            if (selector == null)
            {
                throw Error.ArgumentNull("selector");
            }

            double sum = 0;

            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += selector(source[i]);
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });

            return sum / source.Count;
        }

        /// <summary>
        /// Computes the average of a sequence in parallel.
        /// </summary>
        /// <param name="source">The array to calculate the average of.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The average of the array.</returns>
        public static decimal AverageParallel(this List<decimal> source, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Count == 0)
            {
                throw Error.NoElements();
            }

            decimal sum = 0;

            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => (decimal) 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += source[i];
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });

            return sum / source.Count;
        }

        /// <summary>
        /// Computes the average of values obtained by invoking a transform function on
        /// each element of the input array in parallel.
        /// </summary>
        /// <param name="source">The array to calculate the transformed average of.</param>
        /// <param name="selector">A transform function to apply to each element.</param>
        /// <param name="batchSize">Optional custom batch size to divide work into.</param>
        /// <returns>The average of the array.</returns>
        public static decimal AverageParallel<T>(this List<T> source, Func<T, decimal> selector, int? batchSize = null)
        {
            if (source == null)
            {
                throw Error.ArgumentNull("source");
            }

            if (source.Count == 0)
            {
                throw Error.NoElements();
            }

            if (selector == null)
            {
                throw Error.ArgumentNull("selector");
            }


            decimal sum = 0;

            object LOCK = new object();
            var rangePartitioner = MakePartition(source.Count, batchSize);
            System.Threading.Tasks.Parallel.ForEach(rangePartitioner,
                () => (decimal) 0.0,
                (range, s, acc) =>
                {
                    for (int i = range.Item1; i < range.Item2; i++)
                    {
                        acc += selector(source[i]);
                    }

                    return acc;
                },
                acc =>
                {
                    lock (LOCK)
                    {
                        sum += acc;
                    }
                });

            return sum / source.Count;
        }

        #endregion
    }
}