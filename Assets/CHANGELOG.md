# Changelog
All notable changes to this project will be documented in this file.

## [1.0.3] - 05-10-2019
### Changed
	-Change name partial class to diffirent namespace

## [1.0.0] - 07-09-2019
### Added
	-Linq operator
	-linq parallel
	-linq parallel simd
	-linq simd

	
